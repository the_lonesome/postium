package com.gmkornilov.most_popular.ui

import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview

@Composable
@Preview
fun MostPopularList(
    modifier: Modifier = Modifier
) {
    LazyColumn(
        modifier = modifier
    ) {
    }
}