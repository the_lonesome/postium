package com.gmkornilov.design.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)


object Vk {
    val ForegroundColor = Color(0xFFFFFFFF)
    val BackgroundColor = Color(0xFF0077FF)
}

object Apple {
    val ForegroundColor = Color.White
    val BackgroundColor = Color.Black
}

object Google {
    val ForegroundColor = Color.Unspecified
    val BackgroundColor = Color.White
}

object Facebook {
    val ForegroundColor = Color.White
    val BackgroundColor = Color(0xFF385898)
}